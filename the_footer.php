<?php
/*
Plugin Name: Insert Footer
Description: Allows you to insert code or text in the footer 
Version: 0.1
Author: iamdpegg/emad
*/


if (!class_exists('InsertFooter')) {
  define('PLUGINURL',    plugins_url('', __FILE__));
  wp_register_style('StyleSheet', PLUGINURL . '/style.css');
  wp_enqueue_style('StyleSheet');
  class InsertFooter
  {
    function __construct()
    {
      add_action('init', array(&$this, 'init'));
      add_action('admin_init', array(&$this, 'admin_init'));
      add_action('admin_menu', array(&$this, 'admin_menu'));
      add_action('wp_footer', array(&$this, 'wp_footer'));
    }

    function init()
    {
      load_plugin_textdomain('insert-footer', false, dirname(plugin_basename(__FILE__)) . '/lang');
    }

    function admin_init()
    {
      register_setting('insert-footer', 'if_insert_footer', 'trim');
    }

    function admin_menu()
    {
      add_submenu_page(
        'options-general.php',
        'Insert Footer',
        'Insert Footer',
        'manage_options',
        __FILE__,
        array(&$this, 'options_panel')
      );
    }

    function wp_footer()
    {
      if (!is_admin() && !is_feed() && !is_robots() && !is_trackback()) {
        $text = get_option('if_insert_footer', '');
        $text = convert_smilies($text);
        $text = do_shortcode($text);
        if ($text != '') {
          echo $text, "\n";
        }
      }
    }

    function options_panel()
    {
      ?>
      <div id="ihaf-wrap">
        <div class="wrap">
          <?php screen_icon(); ?>
          <h2>Insert Headers and Footers - Options</h2>
          <div class="if-wrap">
            <form name="dofollow" action="options.php" method="post">
              <?php settings_fields('insert-footer'); ?>

              <label class="if-labels footerlabel" for="if_insert_footer">Scripts in footer:</label>

              <textarea rows="5" cols="57" id="ihaf_insert_footer" name="if_insert_footer"><?php echo esc_html(get_option('if_insert_footer')); ?></textarea><br />
              These scripts will be printed to the <code>&lt;footer&gt;</code> section.

              <p class="submit">
                <input type="submit" name="Submit" value="Save settings" />
              </p>

            </form>
          </div>
        </div>
      </div>
      <?php
    }
  }

  $wp_insert_footer = new InsertFooter();
}
